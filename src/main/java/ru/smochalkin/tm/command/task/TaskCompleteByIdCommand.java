package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-complete-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Complete task by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        serviceLocator.getTaskService().updateStatusById(id, Status.COMPLETED);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
