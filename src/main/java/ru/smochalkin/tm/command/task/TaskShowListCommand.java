package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.SortNotFoundException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final List<Task> tasks;
        @NotNull final String sortName = TerminalUtil.nextLine();
        if (sortName.isEmpty()) {
            tasks = serviceLocator.getTaskService().findAll(userId);
        } else {
            @NotNull final Sort sort = Sort.getSort(sortName);
            System.out.println(sort.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sort.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index++ + ". " + task);
        }
        System.out.println("[OK]");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
