package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

public final class AboutCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String name() {
        return "about";
    }

    @Override
    @NotNull
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        System.out.println("[ABOUT]");
        System.out.println(serviceLocator.getPropertyService().getDeveloperName());
        System.out.println(serviceLocator.getPropertyService().getDeveloperEmail());
    }

}
