package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter project index: ");
        @NotNull final Integer index = TerminalUtil.nextInt();
        if (!serviceLocator.getProjectTaskService().isProjectIndex(userId, index)) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectTaskService().removeProjectByIndex(userId, index);
        System.out.println("[OK]");
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
