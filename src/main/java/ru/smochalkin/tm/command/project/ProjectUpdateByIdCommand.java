package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id);
        System.out.print("Enter new name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        @Nullable final String desc = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateById(id, name, desc);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
