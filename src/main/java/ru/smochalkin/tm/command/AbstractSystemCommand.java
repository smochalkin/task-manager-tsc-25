package ru.smochalkin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    public abstract String arg();

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = super.toString();
        @Nullable final String arg = arg();
        if (arg != null && !arg.isEmpty()) result += " (" + arg + ")";
        return result;
    }

}
