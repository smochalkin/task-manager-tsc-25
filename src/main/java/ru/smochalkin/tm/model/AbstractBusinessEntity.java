package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.model.IWBS;
import ru.smochalkin.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date created = new Date();

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date endDate;

    @Nullable
    protected String userId;

    @Override
    @NotNull
    public String toString() {
        return id + ": " + name + ": " + description +
                "; Status - " + status.getDisplayName() +
                "; Created - " + created +
                "; Start - " + startDate +
                "; End - " + endDate +
                "; userId - " + userId;
    }

}
