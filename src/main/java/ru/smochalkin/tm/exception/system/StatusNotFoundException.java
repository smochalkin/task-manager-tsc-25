package ru.smochalkin.tm.exception.system;

import ru.smochalkin.tm.exception.AbstractException;

public final class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error! Status option is incorrect.");
    }

}
