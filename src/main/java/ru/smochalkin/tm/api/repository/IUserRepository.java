package ru.smochalkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    Boolean isLogin(@NotNull String login);

}
